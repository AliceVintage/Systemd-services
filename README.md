# Custom SystemD Services Collection

This repository contains a curated collection of custom SystemD service files.

## Getting Started

To get started using these services on your own machine, follow these steps:

1. Clone this repository onto your local machine with `git clone https://github.com/AliceVintage/Systemd-services
`.
2. Navigate into the cloned directory with `cd Systemd-services
`.
3. Copy each desired service file from this repository to the appropriate location within your SystemD directories. For example, if copying the "my-service.service" file, use `sudo cp ./my-service.service /etc/systemd/system/`.
4. Enable the copied service by running `sudo systemctl enable my-service`, replacing "my-service" with the name of the actual service being enabled.
5. Restart the newly enabled service with `sudo systemctl restart my-service`.

## Included Services

The following is a list of included custom SystemD services available in this repository:

* **my-service**: Description of what the service does.
* ...

Feel free to submit pull requests adding new services or improving existing ones. Contributions are always welcome!

## License

This project is licensed under the MIT license - see the LICENSE file for details.